<?php

namespace Planet17\MessageQueueLibraryRouteNav\Connections;

use Planet17\MessageQueueLibrary\Connections\ConnectionManagerBase as Origin;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Connections\ManagerInterface;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Resolvers\AliasHandlerResolverInterface;

/**
 * Class Manager
 *
 * @package Planet17\MessageQueueLibraryRouteNav\Connections
 *
 * @method static ManagerInterface getInstance()
 */
abstract class ConnectionManagerBase extends Origin implements ManagerInterface
{
    /** @inheritdoc  */
    abstract public function getResolverAliasHandler(): AliasHandlerResolverInterface;
}
