<?php

namespace Planet17\MessageQueueLibraryRouteNav\Interfaces\Providers;

use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Providers\BaseProviderInterface;

/**
 * Interface HandlersProviderInterface
 *
 * @package Planet17\MessageQueueLibraryRouteNav\Interfaces\Providers
 */
interface HandlersProviderInterface extends BaseProviderInterface
{
    /**
     * Override it with your set.
     *
     * Implement set of Routes for including in map.
     *
     * @return string[]|HandlerInterface[]
     */
    public function provideHandlerClasses(): array;
}