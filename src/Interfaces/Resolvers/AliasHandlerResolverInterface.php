<?php

namespace Planet17\MessageQueueLibraryRouteNav\Interfaces\Resolvers;

use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Providers\RoutesProviderInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteInterface;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Providers\HandlersProviderInterface;

/**
 * Interface RoutedHandlerResolverInterface
 *
 * @package Planet17\MessageQueueLibraryRouteNav\Interfaces\Resolvers
 */
interface AliasHandlerResolverInterface
{
    /**
     * RoutedHandlerResolverInterface constructor.
     *
     * @param RoutesProviderInterface $mapResolverRoutes
     * @param HandlersProviderInterface $mapResolverHandlers
     */
    public function __construct(
        RoutesProviderInterface $mapResolverRoutes,
        HandlersProviderInterface $mapResolverHandlers
    );

    /**
     * Resolve handler by provided route instance.
     *
     * @param RouteInterface $route
     *
     * @return HandlerInterface
     */
    public function resolveHandlerByRoute(RouteInterface $route): HandlerInterface;

    /**
     * Resolver handler by provided route alias.
     *
     * @param string $routeAlias
     *
     * @return HandlerInterface
     */
    public function resolveHandlerByRouteAlias(string $routeAlias): HandlerInterface;
}
