<?php

namespace Planet17\MessageQueueLibraryRouteNav\Interfaces\Connections;

use Planet17\MessageQueueLibraryRouteNav\Interfaces\Resolvers\AliasHandlerResolverInterface;

/**
 * Interface ManagerInterface
 *
 * @package Planet17\MessageQueueLibraryRouteNav\Interfaces\Connections
 *
 * @method static ManagerInterface getInstance() getInstance() multiply two integers
 */
interface ManagerInterface extends \Planet17\MessageQueueLibrary\Interfaces\Connections\ManagerInterface
{
    /**
     * Implement method for getter new resolver's handler of aliases.
     *
     * @return AliasHandlerResolverInterface
     */
    public function getResolverAliasHandler(): AliasHandlerResolverInterface;
}
