<?php

namespace Planet17\MessageQueueLibraryRouteNav\Providers;

use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Providers\HandlersProviderInterface;

/**
 * Class HandlersProvider
 *
 * @package Planet17\MessageQueueLibraryRouteNav\Providers
 *
 * @see HandlersProviderInterface::provideHandlerClasses()
 */
class HandlersProvider implements HandlersProviderInterface
{
    /** @var HandlerInterface[] */
    private $mapped = [];

    /**
     * RoutesMapProvider constructor.
     *
     * Calling method for check preset Routes by methods RoutesProviderInterface::provideRouteClasses() and
     * RoutesProviderInterface::providePackageClasses()
     *
     * @see RoutesProviderInterface::provideRouteClasses()
     * @see RoutesProviderInterface::providePackageClasses()
     */
    final public function __construct()
    {
        $this->analyzeHandlers();
    }

    /** @inheritdoc  */
    public function getMapped(): array
    {
        return $this->mapped;
    }

    /** @inheritdoc */
    public function provideHandlerClasses(): array
    {
        return [];
    }

    /**
     * Return array with key classes and value RouteInterface.
     */
    private function analyzeHandlers(): void
    {
        foreach ($this->provideHandlerClasses() as $provideHandlerClass) {
            /** @var HandlerInterface $instance */
            $instance = new $provideHandlerClass;
            $this->mapped[get_class($instance->getRoute())] = $instance;
        }
    }
}