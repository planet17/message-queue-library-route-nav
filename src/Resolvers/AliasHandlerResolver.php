<?php

namespace Planet17\MessageQueueLibraryRouteNav\Resolvers;

use Planet17\MessageQueueLibrary\Exceptions\Providers\HandlerWithRouteNotFoundException;
use Planet17\MessageQueueLibrary\Exceptions\Providers\InconsistencyProvidersException;
use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Providers\RoutesProviderInterface;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Providers\HandlersProviderInterface;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Resolvers\AliasHandlerResolverInterface;

/**
 * Class RoutedHandlerResolver
 *
 * @package Planet17\MessageQueueLibraryRouteNav\Resolvers
 */
class AliasHandlerResolver implements AliasHandlerResolverInterface
{
    /** @var RoutesProviderInterface $mapResolverRoutes */
    private $mapResolverRoutes;

    /** @var HandlersProviderInterface $mapResolverHandlers */
    private $mapResolverHandlers = null;

    /** @var array $mappedRelationRouteHandler */
    private $mappedRelationRouteHandler = [];

    /** @inheritdoc */
    public function __construct(
        RoutesProviderInterface $mapResolverRoutes,
        HandlersProviderInterface $mapResolverHandlers
    ) {
        $this->mapResolverRoutes = $mapResolverRoutes;
        $this->mapResolverHandlers = $mapResolverHandlers;
        $this->initialize();
    }

    /** @inheritdoc */
    public function resolveHandlerByRoute($route): HandlerInterface
    {
        return $this->resolveHandlerByRouteAlias($route->getAliasFull());
    }

    /** @inheritdoc */
    public function resolveHandlerByRouteAlias($routeAlias): HandlerInterface
    {
        if (!array_key_exists($routeAlias, $this->mappedRelationRouteHandler)) {
            throw new \RuntimeException('Not found route alias: ' . $routeAlias);
        }

        return $this->mappedRelationRouteHandler[$routeAlias];
    }

    /**
     * Method get all map.
     *
     * Compare and set relation via classes and aliases.
     */
    private function initialize(): void
    {
        $mappedRoutes = $this->mapResolverRoutes->getMapped();
        $mappedHandlers = $this->mapResolverHandlers->getMapped();

        if (count($mappedHandlers) > count($mappedRoutes)) {
            throw new InconsistencyProvidersException;
        }

        foreach ($mappedRoutes as $alias => $route) {
            $needleClassHandler = get_class($route);
            if (!array_key_exists($needleClassHandler, $mappedHandlers)) {
                continue;
            }

            $this->mappedRelationRouteHandler[$alias] = $mappedHandlers[$needleClassHandler];
        }
    }
}